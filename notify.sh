curl -s -X POST -H "Content-Type: application/json" -d @- https://api.telegram.org/bot$BOT_KEY/sendMessage << EOF
{
    "chat_id": $CHAT_ID,
    "parse_mode": "Markdown",
    "text": "👩🏽‍💻👨🏽‍💻 Una nueva versión del *sitio web* ha sido publicada\\n\\nhttps://xalapacode.com"
}
EOF
