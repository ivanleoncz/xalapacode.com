+++
name = "José-Clemente Hernández-Hernández"
nickname = "cletsxd"
description = "Tecnólogo, investigador, programador, profesor y soñador intergaláctico"
skills = "Inteligencia Artificial, Python, Procesamiento del Lenguaje Natural, Computación Evolutiva, Redes Neuronales, C/C++, Prolog, LISP"
thumbnail = "/img/directorio/cletsxd.jpg"
draft = false
date = "2024-09-24"
disponibility = "A veces"
rol = "gravity-member"
website = "https://www.linkedin.com/in/j-clemente/"
author = "cletsxd"
+++

Investigador en formación, estudiante de doctorado; programador de humanos (profesor) y de computadoras; desayuno, como y ceno Inteligencia Artificial; tecnólogo; adoro la Astronomía.

**Formación**

* Licenciatura en Tecnologías Computacionales por la Universidad Veracruzana (noviembre 2018)
* Maestría en Inteligencia Artificial por la Universidad Veracruzana (diciembre 2021)
* Doctorado en Inteligencia Artificial por la Universidad Veracruzana (tercer año)

**Estancias de Investigación**

* Centro de Investigación e Innovación en Tecnologías de la Información y Comunicación (INFOTEC) - Responsable: Dr. Mario Graff Guerrero (Aguascalientes, junio - julio 2021)
* Instituto Nacional de Astrofísica Óptica y Electrónica (INAOE) - Departamento de Ciencias de la Computación - Responsable: Dr. Manuel Montes y Gómez (Puebla, enero - junio 2024)
* Instituto de Investigaciones en Matemáticas Aplicadas y Sistemas (IIMAS) - Departamento de Ingeniería de Sistemas Computacionales y Automatización - Responsable: Dra. Helena Gómez Adorno (Ciudad de México, septiembre - diciembre 2024)

**Tesis y artículos**

* Inteligencia Artificial Entrometida. Ciencia y el Hombre 2020. Autores: Guillermo-de-Jesús Hoyos-Rivera, Omar Rodríguez-López y José-Clemente Hernández-Hernández
* Neuroevolution for Sentiment Analysis in Tweets Written in Mexican Spanish. MCPR 2021. Autores: José-Clemente Hernández-Hernández, Efrén Mezura-Montes, Guillermo-de-Jesús Hoyos-Rivera y Omar Rodríguez-López
* Aprendizaje Profundo y Neuroevolución para el Análisis de Sentimientos en Tweets Escritos en Español Mexicano. Tesis de Maestría, 2021. Autor: José-Clemente Hernández-Hernández. Asesores: Guillermo-de-Jesús Hoyos-Rivera y Efrén Mezura-Montes
* Selección de características de representaciones de texto de BETO usando un algoritmo genético. COMIA 2023. Autores: Juan-José Guzmán-Landa, José-Clemente Hernández-Hernández, Guillermo-de-Jesús Hoyos-Rivera y Efrén Mezura-Montes
* Pensamiento Computacional y ChatGPT. SIPECO 2023 - Revista EIA 2024. Guillermo-de-Jesús Hoyos-Rivera y José-Clemente Hernández-Hernández

**Extras**

Doy asesorías de Inteligencia Artificial y tecnologías computacionales. También dirijo tesis relacionadas con la Inteligencia Artificial. Voy a todos lados a dar conferencias de divulgación sobre Inteligencia Artificial

**Curiosidades**

Reprobé primer semestre de Informática en la preparatoria. Segundo semestre de Informática, con el mismo profesor: gracias a él me enamoré de los algoritmos. El resto es historia...

**Hobbies**

Andar en bici, leer ciencia ficción y filosofía, escuchar música synthwave/retrowave/chillwave, rock/metal, post-rock, jugar videojuegos (Fornite, GTA, Euro Truck Simulator, Fall Guys, GRIP Combat Racing, CSGO, Age of Mythology, Minecraft). Me gusta hablar sobre temas profundos, escuchar a la gente y que me hagan ver más allá de lo evidente. Yo también juego peteca :)

_"Si he visto más lejos, es poniéndome sobre los ojos de gigantes"_ Sir Isaac Newton

jclementehdzhdz@gmail.com