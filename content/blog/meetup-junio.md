+++
title = "El meetup de junio de xalapacode"
date = "2019-06-26"
draft = false
thumbnail = ""
description = "Resumén de lo que paso en junio en nuestro meetup"
author = "jail"
author_uri = "/directorio/jail"
+++


El pasado 14 de junio tuvimos meetup en la sala audiovisual de la Facultad de Estadística e Informática de la UV. En este meetup hubo cuatro pláticas que estuvieron muy entretenidas e informativas.

---

## Espagueti y uvas ¿Desayuno para programadores?

En esta plática José Adrián empieza con un relato ficticio de un programador que le piden un nuevo proyecto que se vuelve exitoso para después comenzar a pedirle nuevos requerimientos que no son bien planeados y termina siendo un desastre. Una buena historia de lo que sucede cuando no se planea adecuadamente y cuando no se tiene tiempo para hacer una buena arquitectura.

{{< figure src="/img/blog/meetup_junio_1.jpg" title="Plática de José Adrián: Espagueti y uvas ¿desayuno para programadores " >}}

## Arquitectura multirepo

La primer plática de Diego Matus en nuestra comunidad fue genial, nos platico como usan una arquitectura multirepo en su chamba, los beneficios de usar submodulos en git y como esto les permitía trabajar en diferentes proyectos reutilizando componentes clave.

{{< figure src="/img/blog/meetup_junio_2.jpg" title="Plática de Diego Matus: Arquitectura multirepo " >}}

## Kotlin + Android Architectural Components

Otro que se estrena con nosotros: Daniel García también hablo del tema de componentes y arquitecturas pero desde el punto de vista Android. Las dos principales recomendaciones de esta plática fueron:

* Usa Jetpack si estas empezando desde 0
* Aprende y usa Kotlin porque sera el lenguaje de facto para hacer Android

{{< figure src="/img/blog/meetup_junio_3.jpg" title="Plática de Daniel García: Kotlin + Android Architectural Components" >}}


## El checklist del curriculum

Para finalizar este meetup Lina Beauregard nos trae la lista de requisitos que debe cumplir nuestro CV para aumentar nuestras posibilidades de ser contratados y como debemos enfocarnos en buscar chamba.

{{< figure src="/img/blog/meetup_junio_4.jpg" title="Plática de Lina Beauregard: El Checklist del Curriculum " >}}
