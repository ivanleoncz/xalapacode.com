+++
title = "SHDH Online 2021"
publishdate = "2021-05-13"
date = "2021-05-15"
event_place = "Virtual"
description = "Regresa el SHDH de xalapacode, ahora virtual"
thumbnail = "/img/eventos/shdh-2021-05-15-virtual.jpg"
author = "jail"
topics = []
turnout = 14
+++

Un Super Happy Dev House es un evento de hackers y makers para trabajar en sus proyectos personales durante 12 horas (nosotros lo intentamos) mientras convivimos y al final presentamos lo que hicimos en ese lapso. Es un buen espacio para resolver dudas, compartir inquietudes y obtener retroalimentacion de proyectos de todo tipo y forma.

Los SHDH de xalapacode se realizaran de manera virtual mientras se adapta la nueva normalidad y podemos realizarlo de manera presencial.

## Sobre la sede
El evento se realizara de manera virtual por Zoom y solamente se enviara invitación previo registro de formulario a las 15:00 hrs, tiempo de la Ciudad de México.

# Link de Registro

Te puedes registrar [aqui](https://docs.google.com/spreadsheets/d/1LnpJhQ9eCqb6DJPenTLfuz42xvVUsK6etl6QmOlafzk/edit?usp=sharing)

## Sobre el evento
En un Super Happy Dev House los asistentes se dedican durante 12 horas a desarrollar algún proyecto o varios, aprender alguno nuevo durante el evento, y en las últimas horas se invita a los asistentes a pasar a platicar a los asistentes que es lo que lograron durante el SHDH.

Un Super Happy Dev House no es un evento de networking, no es una competencia o una conferencia (aunque se dan pláticas), no es una fiesta, aunque de alguna manera se haga un poco de todo eso.
