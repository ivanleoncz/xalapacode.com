+++
name = "Ivan Leon"
nickname = "ivanleoncz"
description = "Software Developer"
skills = "Python, C++, Django, Flask, FastAPI, SQL, AWS, Postgresql, ETLs, GNU/Linux <3, Shellscript, Webservices"
thumbnail = "/img/directorio/ivanleoncz.png"
draft = false
date = "2024-08-21"
disponibility = "Remote"
rol = "admin"
website = "https://doadm-notes.blogspot.com/"
author = "ivanleoncz"
+++

Extranjero, orgullosamente parte de esta bella comunidad desde 2018 (aquí encontré trabajo, por cierto).

Desarrollando Software profisionalmente desde 2019, pero en una vida pasada, fuí un GNU/Linux SysAdmin (de los "hardcores").

De lo que sea entre Desarrollo de Software, GNU/Linux, Bases de Datos, Seguridad, y Latino América, Idiomas, Video Juegos y Animes, puede un poco hablar.

Linkedin: https://www.linkedin.com/in/ivanleoncz/

----

- A veces escribo algo aquí: https://doadm-notes.blogspot.com/
- También intento contribuir aquí: https://stackoverflow.com/users/5780109/ivanleoncz?tab=profile
- Algunos de los libros que leí, los tengo apuntados aquí: https://ivanleoncz.pythonanywhere.com/apps/library/
