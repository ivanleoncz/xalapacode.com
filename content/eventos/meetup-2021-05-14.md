+++
title = "Encuentros xalapacode 14 de mayo, 2021"
publishdate = "2021-05-12"
date = "2021-05-14T18:30:00-05:00"
event_place = "En línea"
description = "¡Evento de mayo 2021!"
thumbnail = "/img/eventos/meetup-2021-05-14.png"
author = "jailandrade"
draft = false
+++

## Las pláticas

1. "¿Cómo está afectando la Covid a los objetivos de desarrollo sustentable para 2030?." por **@rrortegaa**
2. "FastAPI, primeras y segundas impresiones." por **@categulario**

## Sobre la sede

Debido a la contingencia sanitaria que se vive a nivel mundial nuestros últimos eventos han sido (y seguirán siendo) en línea, ¡así que ahora es más fácil que nunca asistir!

Sigue la transmisión en vivo por cualquiera de nuestros canales en punto de las 18:30hrs tiempo de la Ciudad de México:

* [Youtube](https://www.youtube.com/watch?v=i_ucYmVP0jI)
* [Facebook live](https://www.facebook.com/158398994507356/posts/1462325710781338/)
* [Twitter](https://twitter.com/xalapacode)

## Sobre el evento

Los encuentros de xalapacode son reuniones en las que se realizan una serie de desconferencias de 20 minutos de duración por miembros de la comunidad como tú sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
