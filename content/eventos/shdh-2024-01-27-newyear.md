+++
title = "SHDH Enero 2024"
publishdate = "2024-01-22"
date = "2024-01-27"
event_place = "Salón 108 en la FEI"
description = "Primer SHDH presencial en 2024!"
thumbnail = "/img/eventos/SHDH_xalapa_azul.jpg"
author = "koffer"
topics = ["SHDH"]
turnout = 14
+++

Te esperamos este Sábado 27 de Enero en el salón 108 de la Facultad de Estadística e
Informática de la Universidad Veracruzana. Trae todo lo que necesites para
trabajar en tu proyecto y ¡ven a convivir!

## Sobre el evento
Un Super Happy Dev House es un evento de hackers y makers para trabajar en sus
proyectos personales durante 12 horas (nosotros lo intentamos) mientras
convivimos y al final presentamos lo que hicimos en ese lapso. Es un buen
espacio para resolver dudas, compartir inquietudes y obtener retroalimentacion
de proyectos de todo tipo y forma.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kZrul_voM00?si=jJ8cRce804vXw9uT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

La dinámica es simple:
¿Tienes un proyecto personal de cualquier tipo y no encuentras el tiempo para
hacerlo? ¡Tráelo!

Para participar registra la actividad que estarás realizando [en este
documento](https://docs.google.com/spreadsheets/d/1V6UhCAAf-3pTQc4WD2CaJ7cG3N_Qc8ASavq5W79nvoA/edit?usp=sharing).
También verás en qué actividades estarán trabajando otras personas.



